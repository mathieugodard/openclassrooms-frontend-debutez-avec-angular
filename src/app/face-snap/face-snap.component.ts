import { Component, OnInit, Input } from '@angular/core';
import { FaceSnap } from '../models/face-snap.model';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit {
  @Input() faceSnap!: FaceSnap;
  snapButtonText!: string;

  ngOnInit() {
    this.snapButtonText = 'Oh snaps!'
  }

  toggleSnap() {
    if(!this.faceSnap.snaped) {
      this.faceSnap.snaps++;
      this.faceSnap.snaped = !this.faceSnap.snaped;
      this.snapButtonText = 'Oops unsnap!'
    } else {
      this.faceSnap.snaps--;
      this.faceSnap.snaped = !this.faceSnap.snaped;
      this.snapButtonText = 'Oh snaps!'
    }
  }
}