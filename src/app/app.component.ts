import { Component, OnInit } from '@angular/core';
import { FaceSnap } from './models/face-snap.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  mySnap!: FaceSnap;

  ngOnInit() {
    this.mySnap = new FaceSnap(
      'Harry Tuttle', 
      "Dans le monde totalitaire de Brazil, Archibald Tuttle est un plombier chauffagiste dissident (« plombier vengeur ») qui intervient illégalement chez les gens pour réparer leurs climatisations. Ses amis l'appellent simplement Harry. Il fait irruption chez Sam Lowry, le personnage central du film, armé d'un Walther P38, dans le but de lui réparer son climatiseur défectueux.",
      new Date(),
      0,
      "./assets/img/harry_tuttle.jpg",
      false
    )
  }
}
